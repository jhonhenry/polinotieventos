import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuRealComponent } from './menu-real.component';

describe('MenuRealComponent', () => {
  let component: MenuRealComponent;
  let fixture: ComponentFixture<MenuRealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuRealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuRealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
