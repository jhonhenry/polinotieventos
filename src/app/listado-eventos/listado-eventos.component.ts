import { Component, OnInit } from '@angular/core';
import { DataService} from '../service/data.service';
import { Evento } from '../model/evento';

@Component({
  selector: 'app-listado-eventos',
  templateUrl: './listado-eventos.component.html',
  styleUrls: ['./listado-eventos.component.css']
})
export class ListadoEventosComponent implements OnInit {
  eventos : Array<Evento>;
  evento:Evento;
  posts =[];

  constructor(private servicio:DataService) { 

    this.servicio.getData().subscribe(data => {
      this.eventos = data;
      
    })
    this.eventos = new Array ();
    this.evento=new Evento();
  }

  addNewTweet () {
    this.eventos.push (this.evento);
    this.evento=new Evento();
  
    }

  ngOnInit() {
  }

  

}
