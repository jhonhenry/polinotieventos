import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {RouterModule, Route} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {misComponentes} from './material';
import { DetalleEventoComponent } from './detalle-evento/detalle-evento.component';
import { ListadoEventosComponent } from './listado-eventos/listado-eventos.component';
import { DataService } from './service/data.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { LoginNavComponent } from './login-nav/login-nav.component';
import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';
import { HomeComponent } from './home/home.component';
import { MenuRealComponent } from './menu-real/menu-real.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

 
const routes: Route[] = [
{path:'',component: LoginNavComponent},

{path:'home',component: HomeComponent}

];


@NgModule({
  declarations: [
    AppComponent,
    DetalleEventoComponent,
    ListadoEventosComponent,
    LoginComponent,
    LoginNavComponent,
    MenuLateralComponent,
    HomeComponent,
    MenuRealComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    misComponentes,
    MatButtonModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgbModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
