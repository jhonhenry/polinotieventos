import { Component, OnInit, Input } from '@angular/core';
import {Evento} from '../model/evento';
import { DataService} from '../service/data.service';

@Component({
  selector: 'app-detalle-evento',
  templateUrl: './detalle-evento.component.html',
  styleUrls: ['./detalle-evento.component.css']
})
export class DetalleEventoComponent implements OnInit {
  @Input() mySelectedEvento:Evento;
  Eventos : Array<Evento>;
  evento:Evento;
  
  constructor() { 

  }

  ngOnInit() {
  }

}
