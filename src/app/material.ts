import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {FormsModule,ReactiveFormsModule  } from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {MatExpansionModule} from '@angular/material/expansion';






@NgModule({
  imports: [MatButtonModule, MatCheckboxModule,MatToolbarModule,MatCardModule,
    MatInputModule,MatTableModule,MatMenuModule,MatIconModule,MatBadgeModule,
    MatButtonToggleModule,MatBottomSheetModule,FormsModule,ReactiveFormsModule,
    MatGridListModule,MatExpansionModule],

  exports: [MatButtonModule,MatToolbarModule,
     MatCheckboxModule,MatCardModule,MatInputModule,
     MatTableModule,MatMenuModule,MatIconModule,MatBadgeModule,
     MatButtonToggleModule,MatBottomSheetModule,FormsModule,ReactiveFormsModule,
     MatGridListModule,ScrollDispatchModule,MatExpansionModule],
})
export class misComponentes { }